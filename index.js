// console.log("Hello World!");

// [SECTION] Functions
// Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked.

/*
		SYNTAX:
			function functionName() {
				code block (statement)
			}
*/

// function keyword - used to define a js function
// functionName - we set name so that we can use it for later
// function block ({}) - this is wher code to be executed

function printName() {
	console.log("My name is John");
}

printName(); //Invocation - calling a function that needs to be executed

function declaredFunction() {
	console.log("Hello World from declaredFunction()")
}

declaredFunction();

// Function Expression
// A function can also be stored in a variable. This is called a function expression.

let variableFunction = function() {
	console.log("Hello Again!");
}

variableFunction();

let funcExpression = function funcName() {
	console.log("Hello from the other side.");
}

funcExpression();

funcExpression = function() {
	console.log("Updated funcExpression")
}

funcExpression();

// Re-assigning declaredFunction() value;

declaredFunction = function() {
	console.log("Updated declaredFunction");
}

declaredFunction();


// Re-assigning function declared with const

const constantFunc = function() {
	console.log("Initial with const")
}

constantFunc();

// Re-assignment of a const Function

/*constantFunc = function() {
	console.log("Can we re-assign it?");
}

constantFunc();*/


// [SECTION] Function Scoping

/*
	Scope is the accessibility (visibility) of variables.
	
	Javascript Variables has 3 types of scope:
		1. local/block scope
		2. global scope
		3. function scope
			JavaScript has function scope: Each function creates a new scope.
			Variables defined inside a function are not accessible (visible) from outside the function.
			Variables declared with var, let and const are quite similar when declared inside a function

*/

{
let localVar = "Armando Perez"
console.log(localVar);
}

// console.log(localVar); --> inaccessible

let globalScope = "Mr. Worldwide";
console.log(globalScope);

// Function Scoping

function showNames() {
	// Function scoped variables:
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionConst);
	console.log(functionLet);
}

showNames();

/*console.log(functionConst);
console.log(functionLet);*/
// --> cannot be used becaused these variables are location inside the scope of a function

// Nested Function
// You can create another function inside a function. This is called a nested function. This nested function, being inside the myNewFunction will have access to the variables, name, as they are within the same scope/code block.

function myNewFunction() {
	let name = "Jane";

	function nestedFunction () {
		let nestedName = "John";
		console.log(name);
	}
	nestedFunction();
}

myNewFunction();
// nestedFunction(); --> will cause error because the function is located to a function scoped parent function.

// Function and Global Scoped Variable

// Global scoped variable

let globalName = "Alexandro";

function myNewFunction2() {
	let nameInside = "Renz";

	// Variables declared Globally (outside any function) have Global Scope.
	// Global variables can be accessed from anywhere in a Javascript.
	// Program including from inside a function.
	console.log(globalName);
}

myNewFunction2();
// console.log(nameInside);

// [SECTION] Using alert()
// alert() allows us to show a small window at the top of our browser page to show information to our users. As opposed to a console.log() which only shows the message on the console. It allows us to show a short dialog or instruction to our user. The page will wait until the user dismisses the dialog.

alert("Hello World!"); //This will be executed immediately

function showAlert() {
	alert("Hello User!");
}

showAlert();

console.log("I will only log in the console when alert is dismissed.")

// Notes on the use of alert():
			//Show only an alert() for short dialogs/messages to the user.
			// Do not overuse alert() because the programs/js has to wait for it to be dismissed before continuing.


// [SECTION] Using prompt()
// prompt() allows us to show a small window at the top of the browser to gather user input. It, much like alert(), will have the page wait until the user completes or enters their input. The input from the prompt() will be returned as a String once the user dismisses the window.

let samplePrompt = prompt("Enter your Name");

console.log("Hello, " + samplePrompt);
console.log(typeof samplePrompt);

// prompt() can be used to gather user input
// prompt() it can be run immediately

function printWelcomeMessage() {
	let firstName = prompt("Enter your first name");
	let lastName = prompt("Enter your last name");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to my page!")
}

printWelcomeMessage();

// [SECTION] Function Naming Convention
// function names should be definitive of the task it will perform. It usually contains a verb. 

function getCourse() {
	let courses = ["Science 101", "Math 101", "English 101"];
	console.log(courses);
}

getCourse();

// AVOID GENERAL NAMES to avoid confusion within your code.

function get(){
	let name = "Jamie";
	console.log(name)
}

get();	

// AVOID POINTLESS and INAPPROPRIATE function names.

function foo(){      //Use another functionName like getModulus
	console.log(25%5);
}

foo();

// Name your functions in small caps. Follow camelCase when naming variables and functions.

// camelCase ---> myNameIsArnel
// snake_case ---> my_name_is_arnel
// kebab-case ---> my-name-is-arnel

function displayCarInfo() {
	console.log("Brand: Toyota");
	console.log("Type: Sedan");
	console.log("Price: 1, 500, 000");
}

displayCarInfo();
